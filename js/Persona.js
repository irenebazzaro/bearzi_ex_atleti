var Persona = /** @class */ (function () {
    function Persona(name, age) {
        this.nome = name;
        this.eta = age;
    }
    // dico che il tipo di dato che mi ritorna è una stringa
    Persona.prototype.getProfile = function () {
        var out = this.nome + " è una persona di " + this.eta + " anni";
        return out;
    };
    return Persona;
}());
//# sourceMappingURL=Persona.js.map