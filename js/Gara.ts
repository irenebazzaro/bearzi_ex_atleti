
class Gara{

    private div_descrizioni:JQuery;
    private pulsante:JQuery;
    public interval_id :number;
    private atleti: Atleta [];
    private atleta_1: Atleta;// in questa maniera inizializzo dicendo che sono elemeti di un array
    private atleta_2: Atleta;
    private atleta_3: Atleta;

    constructor(){
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start").find("input");
        this.pulsante.on("click",()=>this.clickPulsante()); // IMPORTANTE  ()=>this.evento()
        // nota:s ec'è da mettere un parametro si deve mettere sia fra le parentesi tonde
        // prima che quelle dopo  dove se lo aspetta.  Create Field= crea proprietà
        this.initAtleti();
        this.fillDescrizioni();
    };

       private initAtleti(): void {
        var eta:number,velocita:number;
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
        };

    private generaNumeroCasuale(minimo: number, massimo: number, decimali: number=0):number{
    // in questo modo
    // se i decimali sono facoltativi basta che li dichiaro come zero , significa che se non arriva
    //l'istruzione sui decimali di default questi saranno zero
    // quindi non serve scrivere if (typeof decimali === "undefined") {
   // decimali = 0;

    var out:number = NaN;
    var fattore_decimali:number = Math.pow(10,decimali);
    var diff:number = (massimo*fattore_decimali)-(minimo*fattore_decimali);
    var numero:number = Math.random();
    var var_casuale:number = Math.round(numero*diff);
    out = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
    return out;
     };

    private fillDescrizioni(): void {
        for (var i: number = 0; i < this.atleti.length; i++) {
            var atleta: Atleta = this.atleti[i];
            var div: JQuery = this.div_descrizioni.find(".atleta" + (i + 1));
            div.html(atleta.getProfile());
        }
    };


    public clickPulsante():void {
        this.interval_id = setInterval( ()=>this.updateAtleti(),200);
        this.pulsante.off("click");
        this.pulsante.css({ background:"green",
            padding:"2px", width:"100"});
    };

    private updateAtleti(): void {

        var gara_finita: boolean = false;
        for (var i: number = 0; i < this.atleti.length; i++) {
            this.atleti[i].corri(10);
            if (this.atleti[i].metri_percorsi_totali >= 500) {
                gara_finita = true; }
        }
        if (gara_finita)
        {
            clearInterval(this.interval_id);
            this.pulsante.on("click",
                ()=>this.riparti());
            }
    };


    private riparti():void {
        this.reset();
        this.interval_id = setInterval(()=> this.updateAtleti(),200);
        this.pulsante.off("click");
        };


    private reset():void {
        this.initAtleti();
         this.fillDescrizioni();
    };


}
var gara= new Gara();

