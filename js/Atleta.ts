/// <reference path="../dts/jquery/index.d.ts" />



class Atleta extends Persona {
    private velocita: number;
    private div_barra: JQuery;
    private div_nome: JQuery;
    private div_metri: JQuery;
   public metri_percorsi_totali: number = 0;


    constructor(name: string, age: number, speed: number, selector_barra: string) {
        super(name, age);

        this.velocita = speed;
        this.div_barra = $(selector_barra);
        this.div_nome = this.div_barra.find(".nome");
        this.div_metri = this.div_barra.find(".metri");
        this.initDivs();

    }

    private initDivs() {
        this.div_nome.html(this.nome);
        this.div_metri.html(this.metri_percorsi_totali + "mt");
        // questi properties a htmlString sono suggerimenti di IntelliJ non fanno parte
        // del codice
        this.div_barra.css({
            width: "",
            background: ""
        });
    }

    public getProfile(): string {
        var out: string = super.getProfile();
        out += " che corre a " + this.velocita + " metri al secondo";
        return out;
    };

    public corri(secondi: number): void {
        var metri_percorsi_adesso:number = this.velocita * secondi;
        var perdita_mt:number = 0;
        var distanza:number = 0;
        while (distanza <= metri_percorsi_adesso) {
            perdita_mt += (distanza / 10) / 100 * (0.2 * this.eta);
            distanza += 10;
        }
        this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
        this.updateBarra();
    }

    private updateBarra():void {

        if (this.metri_percorsi_totali<500) {

            this.div_metri.html(Math.round(this.metri_percorsi_totali)+"mt");
            this.div_barra.css({
            width: Math.round(this.metri_percorsi_totali/500*100)+"%"
            });
        } else {
            this.div_metri.html("500mt");
            this.div_barra.css({
                width: "100%",
                background: "orange"
            });

    }
}
}
