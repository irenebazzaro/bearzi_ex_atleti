var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// per l'esercizio avro' 3 classi persona,  atleta e gara
// la seconda deriva dalla prima , la terza dalla seconda

//definisco la classe persona

var Persona = (function (){
   function Persona (name, age){
       this.nome = name;
        this.eta = age;
    }
   Persona.prototype.getProfile= function () {
      var out= this.nome+ " è un signore / signora di " + this.eta + " anni "
       return out;
   };
   return Persona;} ());


var Atleta = (function (_super) {
       __extends(Atleta, _super);

    /** costruttore
      */
    function Atleta(name, age, speed, selector_barra) {
         var _this = _super.call(this, name, age) || this;
        // salvo la variabile speed ricevuta nella proprietà "velocita" dell'istanza
        _this.velocita = speed;
               // utilizzo la variabile "selector_barra" per identificare il div della barra
        _this.div_barra = $(selector_barra); // uso Jquery velocizzando la ricerca del dato
        // dal div della barra identifico il div del nome e dei metri attraverso il meotodo "find"
        _this.div_nome = this.div_barra.find(".nome");
        _this.div_metri = this.div_barra.find(".metri");
        // richiamo la funzione che scrive i valori iniziali dei div
        _this.initDivs();
       return _this;
    }

    /** dichiaro alcune proprietà della classe */
    Atleta.prototype.div_barra;
    Atleta.prototype.div_nome;
    Atleta.prototype.div_metri;
    Atleta.prototype.metri_percorsi_totali = 0;

    /** sovrascrive il metodo della classe padre aggiungendone funzionalità
     */
    Atleta.prototype.getProfile = function () {
        /**
         * richiamo il metodo della classe padre
         * e assegno il valore ad una variabile temporanea
         */
        var out = _super.prototype.getProfile.call(this);
         /**
         * ora posso modificare  e aggiungereil risultato ed infine restituirlo
         */
         out +="che corre a "+this.velocita + " metri al secondo";
        return out;
    };

    /**
     * funzione che imposta lo stato iniziale dei div
     * può essere richiamata nel costruttore e ad ogni reset
     */
    Atleta.prototype.initDivs = function () {
        /**
         * attraverso il metodo html di JQuery
         * posso sostituire il contenuto del div
         */
        this.div_nome.html(this.nome);
        this.div_metri.html(this.metri_percorsi_totali + "mt");
          /**
         * reimposto i valori di width e background della barra
         * in modo che al reset non mantengano i valori
         * assegnati da altre funzioni precedentemente e li imposto vuoti
         */
        this.div_barra.css({
            width: "",
            background: ""
        })
    };

    /**
     * funzione che fa avanzare di tot secondi l'atleta
     * @param secondi
     */
    Atleta.prototype.corri = function (secondi) {
        // calcolo i metri percorsi
        var metri_percorsi_adesso =this.velocita*secondi;
        var perdita_mt=0;
        var distanza =0;
       while (distanza<=metri_percorsi_adesso){
           perdita_mt += (distanza/10)/100*(0.2*this.eta);
           distanza +=10;
       }
       this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
       this.updateBarra();
          };

    /**
     * funzione che aggiorna contenuti e aspetto della barra
     */
    Atleta.prototype.updateBarra= function()
    {
    if(this.metri_percorsi_totali<=500){
    this.div_metri.html(Math.round(this.metri_percorsi_totali)+ "mt");
    this.div_barra.css({
        width:math.round(this.metri_percorsi_totali/500 *100)+ "%"});
    // così avanza in percentuale
       } else {this.div_metri.html("500mt");
    this.div_barra.css({width:"100%", background:"#FCB514"});}
    };
    return Atleta;
}(Persona));

var Gara = (function () {
    function Gara() {
        this.div_descrizioni=$("#descrizioni");
        this.pulsante =$("#start input");

        var _this=this;
        this.pulsante.on("click", function () {
            _this.clickPulsante();
        });
   // così ho pietrificato this al valore di questa istanza -
        //assegno la funzione on click al pulsante attraverso il metodo on di Jquery

        // richiamo la funzione che inizializza gli atleti
        this.initAtleti();
        // richiamo la funzione che visualizza le descrizioni degli atleti
        this.fillDescrizioni();
    }
    // dichiaro alcune proprietà della classe
    Gara.prototype.pulsante;
    Gara.prototype.atleta_1;
    Gara.prototype.atleta_2;
    Gara.prototype.atleta_3;
    Gara.prototype.atleti;
    Gara.prototype.div_descrizioni;
    Gara.prototype.interval_id;

    /**
     * funzione che gestisce il primo click sul pulsante start
     */

    Gara.prototype.clickPulsante= function () {
        var _this=this;
        this.interval_id = setInterval( function () {_this.updateAtleti();},500);
        this.pulsante.off("click");
        };

Gara.prototype.initAtleti=function () {
    var eta, velocita;
    eta= this.generaNumeroCasuale(17,55);
    velocita= this.generaNumeroCasuale(2,6,1);
    this.atleta_1= new Atleta ("Giorgio", eta, velocita, "#gara .atleta1");

    eta= this.generaNumeroCasuale(20,50);
    velocita= this.generaNumeroCasuale(3,6,2);
    this.atleta_2= new Atleta ("Pablo", eta, velocita, "#gara .atleta2");

    eta= this.generaNumeroCasuale(22,60);
    velocita= this.generaNumeroCasuale(5,6,1);
    this.atleta_3= new Atleta ("Ettore", eta, velocita, "#gara .atleta3");

    this.atleti = [this.atleta_1, this.atleta_2, this.atleta_3];
}
    Gara.prototype.initAtleti = function () {
        // creo i miei atleti con valori casuali sia nell'eta sia nella velocità usando il
       // metodo generaNumeroCasuale (che ho creato sotto)
        //e poi creounavariabile che li contempla tutti ossia l'array atleti

        var eta,velocita;
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(17,55);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        // creo un'array di atleti per gestirli successivamente attraverso dei loop
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    };

    /**
     * utility per generare numeri casuali con numero minimo, massimo e decimale : da copiare e incollare
     * con uso MATH.POW; MATH.RANDOM; MATH.ROUND
     *
     */
    Gara.prototype.generaNumeroCasuale = function (minimo, massimo, decimali) {
        var out=NaN;
        if(typeof decimali === "undefined"){decimali = 0;}

        var fattore_decimali = Math.pow(10,decimali);
        var diff=(massimo*fattore_decimali)-(minimo*fattore_decimali);
        var numero= Math.random();
        var var_casuale=Math.round(numero*diff);

        out=(((minimo*fattore_decimali)+ var_casuale) /fattore_decimali);
        return out;

         };

    /**
     * funzione che aggiorna gli atleti ad intervalli regolari di 10 in 10
     */
    Gara.prototype.updateAtleti= function () {
       var gara_finita = false;
       for (var i=0; i<this.atleti.length; i++){
           this.atleti[i].corri(10);
           if (this.atleti[i].metri_percorsi_totali>=500){
               gara_finita=true;}
       }
      if (gara_finita) {
           clearInterval(this.interval_id);
           var _this=this;
           this.pulsante.on("click", function () {
               _this.riparti();
           });
      }
    };

    Gara.prototype.riparti= function () {
        this.reset();
        var _this=this;
        this.interval_id=setInterval(function () {
            _this.updateAtleti();
        },500);
        this.pulsante.off("click");
    };
   Gara.prototype.reset= function () {
       this.initAtleti();
       this.fillDescrizioni();
    };


   Gara.prototype.fillDescrizioni= function (){
       for (var i=0; i<this.atleti.length; i++){
           var atleta = this.atleti[i];
           var div=this.div_descrizioni.find( " .atleta" + (i+1));
           div.html(atleta.getProfile());
       }
    };
   return Gara;
   }());

/**
 * creo un'istanza di Gara in modo da far partire l'applicazione
 * senza questa istruzione le definizioni delle classi
 * fatte fin'ora non hanno alcun'effetto
 */
var gara = new Gara();


